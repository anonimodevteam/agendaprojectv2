/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conector;
import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vane
 */
public class Conexion {

    private final static String db = "dbagenda";
    private final static String user = "root";
    private final static String pass = "CursoSql1234";
    private final static String host = "localhost:3306";
    private final static String server ="jdbc:mysql://localhost:3306/" + db;
    private Connection con = null;

    public Connection getConexion() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = (Connection) DriverManager.getConnection(server, user, pass);
     
        } catch (SQLException e) {
            System.out.println(e);
        } catch (ClassNotFoundException ex) 
        {Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }
}
