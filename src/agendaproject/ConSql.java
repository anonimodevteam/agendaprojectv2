/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendaproject;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Empleo
 */
//connecion a la SQL
public class ConSql{
    
    private static String db="dbagenda";                           //base de dato
    private static String user="root";                            //user SQL
    private static String pass="CursoSql1234";                   //contraseña
    private static String host="localhost:3306";                //host
    private static String server="jdbc:mysql://"+host+"/"+db;  //url
    



    public static Connection getConnection(){
    
    Connection con = null;
    
    try{
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection(server,user,pass);
        
    }catch (Exception e){
        System.out.println(e);
    }
    return con;
} 

    public static ResultSet getTabla(String Consult){
    Connection cn=getConnection();
    Statement st;
    ResultSet datos = null;
    try{
        st=cn.createStatement();
        datos=st.executeQuery(Consult);
    }catch(Exception e){
        System.out.println(e.toString());
        
    }
    return datos;
}


}
